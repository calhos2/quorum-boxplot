package Libraries.Compute.Statistics.Distributions

use Libraries.Compute.Math

/*
    This class, ported from Apache Commons, is an implementation of the T-Distribution.
    More information can be found here: https://en.wikipedia.org/wiki/Student%27s_t-distribution
*/
class FDistribution
    constant number DEFAULT_INVERSE_ABSOLUTE_ACCURACY = 0.000000001
    number degreesOfFreedom = 0.0
    number numeratorDegreesOfFreedom = 0.0
    number denominatorDegreesOfFreedom = 0.0
    number solverAbsoluteAccuracy = 0.0
    number factor = 0.0
    number utility = 0.0
    Beta beta
    Math math

    action Setup(number numerator, number denominator)
        if numerator <= 0 or denominator <= 0
            alert("Cannot compute a F-distribution with a degree of freedom value of " + degreesOfFreedom)
        end
        numeratorDegreesOfFreedom = numerator
        denominatorDegreesOfFreedom = denominator
    end

    action CumulativeProbability(number x) returns number
        number ret = 0.0
        if x <= 0.0
            ret = 0.0
        else
            number n = numeratorDegreesOfFreedom
            number m = denominatorDegreesOfFreedom

            number f = beta:RegularizedBeta(
                    n * x / (m + n * x),
                    0.5 * n,
                    0.5 * m)

            return f
        end
        return ret
    end
end